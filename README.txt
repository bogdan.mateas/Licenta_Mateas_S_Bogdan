Investigating the use of containers and microservices in a web application

Instructiuni

1.Asigurati-va ca aveti docker instalat pe sistemul dvs si este deschis.

2.Dupa navigati din terminal in directorul app al aplicatiei si rulati comanda: docker-compose build --no-cache
3.Tot din directorul app rulati comanda: docker-compose up
4.In cazul in care containerele nu au pornit toate deodate mergeti din interfata Docker si reporniti toate containerele.
5.Dupa aplicatia ar trebuii sa fie disponibila la adresa: http://localhost:5000/
